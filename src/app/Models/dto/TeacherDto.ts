export class TeacherDto {
    Id: string
    FirstName: string
    LastName: string
    NeighborhoodCode: number
    Street: string
    BuildingNumber: number
    Phone: string
    Email: string
    Password: string
    IsAuthorized: boolean
    ErrorMessage: string
    OrganizationName: string
}