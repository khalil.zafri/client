export class YearForTeacherDto {
    Id: number
    TeacherId: number
    Year: number
    OrganizationCode: number
}