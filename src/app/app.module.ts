import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { rootRouterConfig } from './app-routing.module';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { LoginComponent } from './component/login/login.component';
import { UserComponent } from './component/user/user.component';
import { RegisterComponent } from './component/register/register.component';
import { UserResolver } from './component/user/user.resolver';
import { AuthGuard } from './core/auth.guard';
import { AuthService } from './core/auth.service';
import { UserService } from './core/user.service';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import {  MaterialModule} from '../app/Material';
import {PrimengModule} from '../app/primeng';
import {FlexLayoutModule} from "@angular/flex-layout";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AlifeFileToBase64Module } from 'alife-file-to-base64';
import {StarRatingModule} from 'angular-star-rating';
//import { RatingModule } from 'ng-starrating';
import { BarRatingModule } from "ngx-bar-rating";

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { SearchComponent } from './component/search/search.component';
import { HomeComponent } from './component/home/home.component';
import { EditConstraintComponent } from './component/edit-constraint/edit-constraint.component';
import { ExchangeComponent } from './component/exchange/exchange.component';
//import { NgbTimepicker } from '../../node_modules/@ng-bootstrap/ng-bootstrap/timepicker/timepicker';

//calandar
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { MyCalandarComponent } from './component/my-calandar/my-calandar.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserComponent,
    RegisterComponent,
    SearchComponent,
    HomeComponent,
    EditConstraintComponent,
    ExchangeComponent,
    MyCalandarComponent,
    
  ],
  
  imports: [
    
    NgbModule,
     BrowserModule,
     FormsModule,
    MaterialModule,
    AlifeFileToBase64Module,
    PrimengModule,
    HttpClientModule,
    StarRatingModule.forRoot(),
  // RatingModule,
   BarRatingModule,
   //NgbTimepicker,
    FlexLayoutModule,
   
    ReactiveFormsModule,
    RouterModule.forRoot(rootRouterConfig, { useHash: false }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features
    //calandar
    BrowserAnimationsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ],
  providers: [AuthService, UserService, UserResolver, AuthGuard,{ provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: [] },],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
