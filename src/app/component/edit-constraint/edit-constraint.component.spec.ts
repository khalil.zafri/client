import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditConstraintComponent } from './edit-constraint.component';

describe('EditConstraintComponent', () => {
  let component: EditConstraintComponent;
  let fixture: ComponentFixture<EditConstraintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditConstraintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditConstraintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
