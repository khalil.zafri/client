import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../core/auth.service'
import { Router, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TeacherDto } from 'src/app/Models';
import { Menu, MenuItem } from 'primeng/primeng';
import { componentRefresh } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  loginForm: FormGroup;
  errorMessage: string = '';

  loginSucceed = true;
  constructor(
    public authService: AuthService,
    private router: Router,
    private fb: FormBuilder

  ) {
    this.createForm();
  }

  createForm() {
    this.loginForm = this.fb.group({
      email: ['', Validators.required ],
      password: ['',Validators.required]
    });
  }

  // tryFacebookLogin(){
  //   this.authService.doFacebookLogin()
  //   .then(res => {
  //     this.router.navigate(['/user']);
  //   })
  // }

  // tryTwitterLogin(){
  //   this.authService.doTwitterLogin()
  //   .then(res => {
  //     this.router.navigate(['/user']);
  //   })
  // }

  tryGoogleLogin(){
    this.authService.doGoogleLogin()
    .then(res => {
      // this.myService.getUserFromServer().subscribe(
      //           user => { 
      //             this.myService.user = user,
      //              this.myService.dataSource=user,
                   
                  
      //               err => console.error('Observer got an error: ' + err)
        
      //           });
     // window.location.reload();
       
      this.router.navigate(['/user']);
    })
  }

  tryLogin(value){
    this.authService.doLogin(value)
    .then(res => {
      this.router.navigate(['/user']);
    }, err => {
      console.log(err);
      this.errorMessage ="אין רשומת משתמש המתאימה למזהה זה. באפשרותך להירשם.";
      
    })
  }

}
