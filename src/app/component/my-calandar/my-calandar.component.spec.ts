import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyCalandarComponent } from './my-calandar.component';

describe('MyCalandarComponent', () => {
  let component: MyCalandarComponent;
  let fixture: ComponentFixture<MyCalandarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyCalandarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyCalandarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
