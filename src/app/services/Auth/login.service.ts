import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from "@angular/router";
import { TeacherDto } from "../../models"
import { AppSettings } from "src/app/AppSettings";
const ApiBaseRoute = AppSettings.API_ENDPOINT + "Login/";

@Injectable({
    providedIn: 'root'
  })
export class LoginService  {
   email:string;
    constructor(private http: HttpClient) {}

 
    getTeacherByEmail( ) : Observable<TeacherDto>
    {
        this.email= JSON.parse(localStorage.getItem('email'));

        return this.http.get<TeacherDto>(ApiBaseRoute+"GetTeacherByEmail?email="+this.email);
    }


}

