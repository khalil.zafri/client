import * as core from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { TeacherScheduleDto, ExchangingDto } from "../../models"
import { HttpParams } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { AppSettings } from "src/app/AppSettings";

const ApiBaseRoute = AppSettings.API_ENDPOINT + "Schedule/";

@Injectable({
    providedIn: 'root'
  })export class ScheduleService {

    TeacherSchedule: TeacherScheduleDto[];
    constructor(private http: HttpClient) {
       
    }

    
    public getSchedule(year:string,teacherId:string): Observable<TeacherScheduleDto[]> {
      
       return this.http.get<TeacherScheduleDto[]>(ApiBaseRoute+"GetTeacherSchedule?year="+ year+"&teacherId="+teacherId);
      }

}