
export const environment = {
  production: false,
  
	apiServer: {
		port: 1234,
		useHttps: false,
		serverUrl: 'localhost',
		fullAddress: function() {
			let protocol = environment.apiServer.useHttps ? 'https' : 'http';
			let address = environment.apiServer.serverUrl;
			let port = environment.apiServer.port && environment.apiServer.port > 0 ? `:${environment.apiServer.port}` : '';
	
			return `${protocol}://${address}${port}/`;
		}
	},
  firebase: {
    apiKey: "AIzaSyD836pkFAPyhmWRFROkBthE2XCm-MimGjI",
    authDomain: "smartschedule-377d8.firebaseapp.com",
    databaseURL: "https://smartschedule-377d8.firebaseio.com",
    projectId: "smartschedule-377d8",
    storageBucket: "smartschedule-377d8.appspot.com",
    messagingSenderId: "55461168336"
   

  }
};


